/*

OBJECTIVE: This script aims to synchronise all logins across the availability group ensuring SIDs match.
		   A non-match means the REPLICA doesn't have the login this server has.

		   After execution check the 'messages' section and the code has been generated to duplicate the login.

		   THIS MUST BE RUN ON EACH REPLICA!

AUTHOR: Aaron Mussig
DATE: 12/08/2015

*/

-- SETTINGS
SET NOCOUNT ON

-- DECLARE VARIABLES

DECLARE @LoginCursor CURSOR;
DECLARE	@SID VARBINARY(85)
DECLARE @NAME SYSNAME;


-- LOOP THROUGH EACH ROW

BEGIN

	SET @LoginCursor = CURSOR FOR
		SELECT sid, name FROM master.sys.server_principals WHERE type IN ('G', 'U', 'S') AND NAME NOT LIKE '%##%';

	OPEN @LoginCursor
		FETCH NEXT FROM @LoginCursor INTO @SID, @NAME

	WHILE @@FETCH_STATUS = 0
	BEGIN

		-- ACTION HERE

		-- CHECK SDC01
		IF (@@SERVERNAME <> 'SDCWNPRDB01\OLTP14EEPR01')
		BEGIN

			IF ((SELECT COUNT(name) FROM [SDCWNPRDB01\OLTP14EEPR01].[master].[sys].[server_principals] WHERE sid = @SID AND name = @NAME) <> 1)
			BEGIN
			
				SELECT 'SDCWNPRDB01 is missing the login: ' + CONVERT(NVARCHAR(MAX), @NAME)

				exec sp_help_revlogin @LOGIN_NAME = @NAME

			END;

		END;

		-- CHECK SDC02
		IF (@@SERVERNAME <> 'SDCWNPRDB02\OLTP14EEPR01')
		BEGIN

			IF ((SELECT COUNT(name) FROM [SDCWNPRDB02\OLTP14EEPR01].[master].[sys].[server_principals] WHERE sid = @SID AND name = @NAME) <> 1)
			BEGIN
			
				SELECT 'SDCWNPRDB02 is missing the login: ' + CONVERT(NVARCHAR(MAX), @NAME)

				exec sp_help_revlogin @LOGIN_NAME = @NAME

			END;
		
		END;
		

		-- CHECK FBR01
		IF (@@SERVERNAME <> 'FBRWNPRDB01\OLTP14EEPR01')
		BEGIN

			IF ((SELECT COUNT(name) FROM [FBRWNPRDB01\OLTP14EEPR01].[master].[sys].[server_principals] WHERE sid = @SID AND name = @NAME) <> 1)
			BEGIN
			
				SELECT 'FBRWNPRDB01 is missing the login: ' + CONVERT(NVARCHAR(MAX), @NAME)

				exec sp_help_revlogin @LOGIN_NAME = @NAME

			END;

		END;

		-- CHECK FBR02
		IF (@@SERVERNAME <> 'FBRWNPRDB02\OLTP14EEPR01')
		BEGIN

			IF ((SELECT COUNT(name) FROM [FBRWNPRDB02\OLTP14EEPR01].[master].[sys].[server_principals] WHERE sid = @SID AND name = @NAME) <> 1)
			BEGIN
			
				SELECT 'FBRWNPRDB02 is missing the login: ' + CONVERT(NVARCHAR(MAX), @NAME)

				exec sp_help_revlogin @LOGIN_NAME = @NAME

			END;
		
		END;

		-- LOOP TO NEXT

		FETCH NEXT FROM @LoginCursor INTO @SID, @NAME

	END;

	CLOSE @LoginCursor;
	DEALLOCATE @LoginCursor;

END;
